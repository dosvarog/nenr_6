#!/usr/bin/env python3.2
# -*- coding: utf-8 -*-
#
#  anfis.py
#
#  Copyright 2015 domagoj <domagoj@domagoj-thinkpad>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import numpy as np
from samplereader import SampleReader
from samplegenerator import SampleGenerator


def sigmoid(a, b, x):
    return 1 / (1 + np.exp(b * (x - a)))


class ANFIS(object):

    def __init__(self, num_of_rules=4):
        """
        :type num_of_rules: number of rules for ANFIS network
        """
        self.num_of_rules = num_of_rules
        self.a = np.random.uniform(-1, 1, size=num_of_rules)
        self.b = np.random.uniform(-1, 1, size=num_of_rules)
        self.c = np.random.uniform(-1, 1, size=num_of_rules)
        self.d = np.random.uniform(-1, 1, size=num_of_rules)
        self.p = np.random.uniform(-1, 1, size=num_of_rules)
        self.q = np.random.uniform(-1, 1, size=num_of_rules)
        self.r = np.random.uniform(-1, 1, size=num_of_rules)

        self.derivative_a = np.zeros(self.num_of_rules)
        self.derivative_b = np.zeros(self.num_of_rules)
        self.derivative_c = np.zeros(self.num_of_rules)
        self.derivative_d = np.zeros(self.num_of_rules)
        self.derivative_p = np.zeros(self.num_of_rules)
        self.derivative_q = np.zeros(self.num_of_rules)
        self.derivative_r = np.zeros(self.num_of_rules)

    def _set_derivatives_to_zero(self):
        self.derivative_a = np.zeros(self.num_of_rules)
        self.derivative_b = np.zeros(self.num_of_rules)
        self.derivative_c = np.zeros(self.num_of_rules)
        self.derivative_d = np.zeros(self.num_of_rules)
        self.derivative_p = np.zeros(self.num_of_rules)
        self.derivative_q = np.zeros(self.num_of_rules)
        self.derivative_r = np.zeros(self.num_of_rules)

    def _feed_forward(self, input_sample):

        # fuzzification layer (1. layer)
        alpha = sigmoid(self.a, self.b, input_sample[0])
        beta = sigmoid(self.c, self.d, input_sample[1])

        # rule layer (2. layer)
        rules = alpha * beta

        # normalisation layer (3. layer)
        sum_of_weights = np.sum(rules)
        normalisation = rules / sum_of_weights

        # defuzzification layer (4. layer)
        z = self._calculate_z(input_sample[0], input_sample[1], self.p, self.q,
                              self.r)
        f = z * normalisation

        # summation layer (5. layer)
        output = np.sum(f)

        return dict({'alpha': alpha, 'beta': beta, 'rules': rules,
                     'sum_of_weights': sum_of_weights,
                     'normalisation': normalisation, 'z': z, 'f': f,
                     'output': output})

    def _calculate_z(self, x, y, p, q, r):
        return p * x + q * y + r

    def _calculate_error(self, dataset):

        error_sum = 0
        for datum in dataset:
            error_sum = (datum[2] - self.predict(datum))**2

        return error_sum / len(dataset)

    def _update_parameters(self, learning_rate, derivatives):
        self.a -= learning_rate * derivatives[0]
        self.b -= learning_rate * derivatives[1]
        self.c -= learning_rate * derivatives[2]
        self.d -= learning_rate * derivatives[3]
        self.p -= learning_rate * derivatives[4]
        self.q -= learning_rate * derivatives[5]
        self.r -= learning_rate * derivatives[6]

        self._set_derivatives_to_zero()

    def predict(self, datum):
        # return output of network
        return self._feed_forward(datum)['output']

    def fit(self, dataset, learning_rate=1e-4, epochs=10000, batch=False):

        epoch = 0
        while True:
            epoch += 1
            error = self._calculate_error(dataset)
            print("Epoch:", epoch)
            print("Error:", error)

            for datum in dataset:
                network_parameters = self._feed_forward(datum)
                sum_of_weights = network_parameters['sum_of_weights']
                output_error = datum[2] - network_parameters['output']

                for i in range(self.num_of_rules):
                    sum_rules_z = np.sum(network_parameters['rules'] * (
                        network_parameters['z'][i] - network_parameters['z']))
                    ab = ((output_error * sum_rules_z *
                        network_parameters['beta'][i] *
                        network_parameters['alpha'][i] * (1 -
                        network_parameters['alpha'][i])) /
                        sum_of_weights**2)
                    self.derivative_a[i] -= ab * self.b[i]
                    self.derivative_b[i] += ab * (datum[0] - self.a[i])

                    cd = ((output_error * sum_rules_z *
                        network_parameters['alpha'][i] *
                        network_parameters['beta'][i] * (1 -
                        network_parameters['beta'][i])) /
                        sum_of_weights**2)
                    self.derivative_c[i] -= cd * self.d[i]
                    self.derivative_d[i] += cd * (datum[1] - self.c[i])

                    pqr = ((output_error * network_parameters['rules'][i]) /
                           sum_of_weights)
                    self.derivative_p[i] -= pqr * datum[0]
                    self.derivative_q[i] -= pqr * datum[1]
                    self.derivative_r[i] -= pqr

                    derivatives = [self.derivative_a, self.derivative_b,
                                   self.derivative_c, self.derivative_d,
                                   self.derivative_p, self.derivative_q,
                                   self.derivative_r]

                if not batch:
                    self._update_parameters(learning_rate, derivatives)

            if batch:
                self._update_parameters(learning_rate, derivatives)

            if error <= 1e-4 or epoch == epochs:
                break

if __name__ == '__main__':
    sg = SampleGenerator()
    sg.generate()
    ds = SampleReader.read_dataset()
    anfis = ANFIS(8)
    anfis.fit(ds, batch=True)
    for d in ds:
        prediction = anfis.predict(d)
        print(prediction)
