#!/usr/bin/env python3.2
# -*- coding: utf-8 -*-
#
#  samplegenerator.py
#
#  Copyright 2015 domagoj <domagoj@domagoj-thinkpad>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from math import cos


class SampleGenerator(object):

    def __init__(self, x=(-4, 4), y=(-4, 4), output_file='out.txt'):
        self.x = list(range(x[0], x[1] + 1))
        self.y = list(range(y[0], y[1] + 1))
        self.output_file = output_file

    def generate(self):

        samples = [str(x) + ' ' + str(y) + ' ' + str(self.function(x, y))
                   for x in self.x for y in self.y]

        with open(self.output_file, 'w') as f:
            for sample in samples:
                f.write(sample + '\n')

    def function(self, x, y):

        return ((x - 1)**2 + (y + 2)**2 - 5 * x * y + 3) * cos(x / 5)**2

if __name__ == '__main__':
    sg = SampleGenerator()
    sg.generate()
