#!/usr/bin/env python3.2
# -*- coding: utf-8 -*-
#
#  samplereader.py
#
#  Copyright 2015 domagoj <domagoj@domagoj-thinkpad>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


class SampleReader(object):

    @classmethod
    def read_dataset(cls, filename='out.txt'):
        """ Helper method that reads dataset from file. Each row is one list.
            Whole dataset is list of lists.
        """

        with open(filename) as f:
            dataset = [[float(elem) for elem in line.split()] for line in f]

        return dataset

if __name__ == '__main__':
    pass
    # ds = SampleReader.read_dataset()
    # print(ds)
